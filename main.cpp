#include <iostream>
#include <ctime>

using namespace std;

void printArray(int* array, int size)
{
    for(int i = 0; i < size; i++)
        cout << array[i] << " ";
    cout << "\n";
}

void createArray(int* array, int size)
{
    srand(time(NULL));
    for(int i = 0; i < size; i++)
        array[i] = rand() % 10;
    printArray(array, size);
}

void sillyPrintArray(int* array, int size, int begin)
{
    int i = begin, j = 0;

    while (j < size)
    {
        if(i < 0) i = size - 1;
        cout << array[i] << " ";
        i--;
        j++;
    }
    cout << "\n";
}

void sortArray(int* array, int size)
{
    int tmp;

    for(int i = 0; i < size - 1; i++)
    {
        for(int j = 0; j < size - 1; j++)
        {
            if(array[j + 1] > array[j])
            {
                tmp = array[j + 1];
                array[j + 1] = array[j];
                array[j] = tmp;
            }
        }
    }
    printArray(array, size);
}

int deleteOdd(int* array, int size)
{
    int newSize = size;

    for(int i = 0; i < newSize; i++)
    {
        if(array[i] % 2 == 0)
        {
            for(int j = i; j < newSize - 1; j++)
            {
                array[j] = array[j + 1];
            }
            i--;
            newSize--;
        }
    }
    printArray(array, newSize);

    return newSize;
}

int main()
{
    const int maxArraySize = 100;
    int array[maxArraySize], size, k;

    do
    {
        system("cls");
        cout << "Enter the size of array (1.." << maxArraySize << "): ";
        cin >> size;
    } while(size > maxArraySize || size < 1);
    createArray(array, size);

    do
    {
        cout << "\nEnter the beginning of the array output (0.." << size - 1 << "): ";
        cin >> k;
    } while(k < 0 || k >= size);
    sillyPrintArray(array, size, k);

    cout << "\nSort array:\n";
    sortArray(array, size);

    cout << "\nDelete odd numbers:\n";
    size = deleteOdd(array, size);

    system("pause");
    return 0;
}
